﻿/*
* Copyright (c) 2020 MemoryInterop.ILPatcher - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using System;

namespace MemoryInterop.TestCommon
{
    /// <summary>
    /// A simple memory helper implementation using all the stub methods so we can do some unit testing.
    /// </summary>
    public static class MemoryHelper
    {
        public static ref T AsRef<T>(IntPtr pSrc) where T : struct
        {
            return ref MemoryInterop.AsRef<T>(pSrc);
        }

        public static ref TTo As<TFrom, TTo>(ref TFrom src) where TFrom : struct where TTo : struct
        {
            return ref MemoryInterop.As<TFrom, TTo>(ref src);
        }

        public static ref readonly TTo AsReadonly<TFrom, TTo>(in TFrom src) where TFrom : struct where TTo : struct
        {
            return ref MemoryInterop.AsReadonly<TFrom, TTo>(in src);
        }

        public static void Write<T>(IntPtr pDst, T[] data, int startIndexInArray, int count) where T : struct
        {
            MemoryInterop.WriteArray<T>(pDst, data, startIndexInArray, count);
        }

        public static void WriteUnaligned<T>(IntPtr pDst, T[] data, int startIndexInArray, int count) where T : struct
        {
            MemoryInterop.WriteArrayUnaligned<T>(pDst, data, startIndexInArray, count);
        }

        public static void Read<T>(IntPtr pSrc, T[] data, int startIndexInArray, int count) where T : struct
        {
            MemoryInterop.ReadArray<T>(pSrc, data, startIndexInArray, count);
        }

        public static void ReadUnaligned<T>(IntPtr pSrc, T[] data, int startIndexInArray, int count) where T : struct
        {
            MemoryInterop.ReadArrayUnaligned<T>(pSrc, data, startIndexInArray, count);
        }

        public static int SizeOf<T>() where T : struct
        {
            return MemoryInterop.SizeOfInline<T>();
        }

        public static IntPtr AsPointer<T>(ref T src) where T : struct
        {
            return MemoryInterop.AsPointerInline<T>(ref src);
        }

        public static IntPtr AsPointerReadonly<T>(in T src) where T : struct
        {
            return MemoryInterop.AsPointerReadonlyInline<T>(in src);
        }

        public static unsafe void Write<T>(IntPtr pDst, in T src) where T : struct
        {
            MemoryInterop.WriteInline<T>((void*) pDst, in src);
        }

        public static unsafe void WriteUnaligned<T>(IntPtr pDst, in T src) where T : struct
        {
            MemoryInterop.WriteUnalignedInline<T>((void*) pDst, in src);
        }

        public static unsafe void Read<T>(IntPtr pSrc, out T dst) where T : struct
        {
            dst = MemoryInterop.ReadInline<T>((void*) pSrc);
        }

        public static unsafe T Read<T>(IntPtr pSrc) where T : struct
        {
            return MemoryInterop.ReadInline<T>((void*) pSrc);
        }

        public static unsafe void ReadUnaligned<T>(IntPtr pSrc, out T dst) where T : struct
        {
            dst = MemoryInterop.ReadUnalignedInline<T>((void*) pSrc);
        }

        public static unsafe T ReadUnaligned<T>(IntPtr pSrc) where T : struct
        {
            return MemoryInterop.ReadUnalignedInline<T>((void*) pSrc);
        }

        public static unsafe void MemoryCopy(IntPtr pDst, IntPtr pSrc, uint byteCount)
        {
            MemoryInterop.MemCopyInline((void*) pDst, (void*) pSrc, byteCount);
        }

        public static unsafe void MemoryCopyUnaligned(IntPtr pDst, IntPtr pSrc, uint byteCount)
        {
            MemoryInterop.MemCopyUnalignedInline((void*) pDst, (void*) pSrc, byteCount);
        }

        public static unsafe void ClearMemory(IntPtr ptr, byte clearValue, uint byteCount)
        {
            MemoryInterop.MemSetInline((void*) ptr, clearValue, byteCount);
        }

        public static unsafe void ClearMemoryUnaligned(IntPtr ptr, byte clearValue, uint byteCount)
        {
            MemoryInterop.MemSetUnalignedInline((void*) ptr, clearValue, byteCount);
        }
    }
}
