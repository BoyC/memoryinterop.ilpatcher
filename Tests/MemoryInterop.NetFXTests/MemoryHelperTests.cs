﻿/*
* Copyright (c) 2020 MemoryInterop.ILPatcher - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using MemoryInterop.TestCommon;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Xunit;
using Xunit.Abstractions;

namespace MemoryInterop.NetFXTests
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Int3
    {
        public int X;
        public byte Y;
        public int Z;

        public Int3(int x, byte y, int z) { X = x; Y = y; Z = z; }

        public static void AssertEquals(Int3 expected, Int3 actual)
        {
            Assert.Equal(expected.X, actual.X);
            Assert.Equal(expected.Y, actual.Y);
            Assert.Equal(expected.Z, actual.Z);
        }
    }

    public class MemoryHelperTests
    {
        private readonly ITestOutputHelper m_output;

        public MemoryHelperTests(ITestOutputHelper output)
        {
            m_output = output;
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public void MultipleInlinedMethodTest()
        {
            int val;
            AddFive(12, out val);

            Assert.Equal(17, val);
        }

        private static unsafe void AddFive(int startVal, out int endVal)
        {
            IntPtr ptr = MemoryInterop.TestCommon.MemoryInterop.AsPointerInline<int>(ref startVal);

            startVal += 5;

            endVal = MemoryInterop.TestCommon.MemoryInterop.ReadInline<int>((void*)ptr);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public void AsPointerTest()
        {
            double val = 52.15;
            double expected = val + 2.51562;

            IntPtr ptr = MemoryHelper.AsPointer<double>(ref val);
            MemoryHelper.Write<double>(ptr, in expected);

            Assert.Equal(expected, val);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public void AsPointerReadonlyTest()
        {
            double val = 52.15;
            double expected = val + 2.51562;

            //Yes...we basically casted away the constness... !
            IntPtr ptr = MemoryHelper.AsPointerReadonly<double>(in val);
            MemoryHelper.Write<double>(ptr, in expected);

            Assert.Equal(expected, val);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void AsRefTest()
        {
            double* vl = stackalloc double[1];
            vl[0] = 52;

            ref double refVl = ref MemoryHelper.AsRef<double>(new IntPtr(vl));

            Assert.Equal(52, refVl);

            refVl = 189.25;

            Assert.Equal(189.25, vl[0]);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void AsTest()
        {
            byte[] b = new byte[4] { 0x42, 0x42, 0x42, 0x42 };

            ref int ivl = ref MemoryHelper.As<byte, int>(ref b[0]);

            Assert.Equal(0x42424242, ivl);

            ivl = 0x0EF00EF0;
            Assert.Equal(0xFE, b[0] | b[1] | b[2] | b[3]);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void AsReadonlyTest()
        {
            byte[] b = new byte[4] { 0x42, 0x42, 0x42, 0x42 };

            ref readonly int ivl = ref MemoryHelper.AsReadonly<byte, int>(in b[0]);

            Assert.Equal(0x42424242, ivl);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public void SizeOfTest()
        {
            int size = MemoryHelper.SizeOf<Int3>();

            int marshalSize = Marshal.SizeOf(typeof(Int3));

            Assert.Equal(size, marshalSize);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void MemSetTest()
        {
            uint numBytes = sizeof(int);
            byte* stackPtr = stackalloc byte[(int)numBytes];

            IntPtr ptr = new IntPtr(stackPtr);

            byte expected = 35;
            MemoryHelper.ClearMemory(ptr, expected, numBytes);

            for (int i = 0; i < numBytes; i++)
                Assert.Equal(expected, stackPtr[i]);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void MemSetUnalignedTest()
        {
            uint numBytes = sizeof(int) + 1;
            byte* stackPtr = stackalloc byte[(int)numBytes];
            stackPtr += 1;

            IntPtr ptr = new IntPtr(stackPtr);

            byte expected = 35;
            MemoryHelper.ClearMemoryUnaligned(ptr, expected, numBytes);

            for (int i = 0; i < numBytes; i++)
                Assert.Equal(expected, stackPtr[i]);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void ReadTest()
        {
            int expected = 10;
            IntPtr ptr = MemoryHelper.AsPointer<int>(ref expected);

            int ret = MemoryHelper.Read<int>(ptr);

            int ret2;
            MemoryHelper.Read<int>(ptr, out ret2);

            Assert.Equal(expected, ret);
            Assert.Equal(expected, ret2);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void ReadUnalignedTest()
        {
            int expected = 10;
            IntPtr ptr = MemoryHelper.AsPointer<int>(ref expected);

            int ret = MemoryHelper.ReadUnaligned<int>(ptr);

            int ret2;
            MemoryHelper.ReadUnaligned<int>(ptr, out ret2);

            Assert.Equal(expected, ret);
            Assert.Equal(expected, ret2);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void ReadWriteUnalignedTest()
        {
            //Test writing/reading from an unaligned ptr using unaligned methods

            uint numBytes = sizeof(int) + 1;
            byte* stackPtr = stackalloc byte[(int)numBytes];
            stackPtr += 1;

            IntPtr ptr = new IntPtr(stackPtr);
            MemoryHelper.ClearMemoryUnaligned(ptr, 0, numBytes);

            int expected = 10;
            MemoryHelper.WriteUnaligned<int>(ptr, in expected);

            int ret = MemoryHelper.ReadUnaligned<int>(ptr);
            int ret2;
            MemoryHelper.ReadUnaligned<int>(ptr, out ret2);

            Assert.Equal(expected, ret);
            Assert.Equal(expected, ret2);
            Assert.Equal(expected, ret2);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void ReadWriteUnalignedTest2()
        {
            //Test writing/reading from an unaligned ptr using unaligned methods + a more interesting struct

            //Array for 4 structs, with some padding
            uint numBytes = (uint) (MemoryHelper.SizeOf<Int3>() * 4) + 1;
            void* stackPtr = stackalloc byte[(int)numBytes];

            IntPtr ptr = new IntPtr(stackPtr);
            ptr += 1; //Increment by amount of padding
            int sizeInBytes = MemoryHelper.SizeOf<Int3>();

            MemoryHelper.ClearMemoryUnaligned(ptr, 0, numBytes);

            ptr += (sizeInBytes * 2); //Go to "index 2"

            Int3 expected = new Int3(2005, 35, 1015);

            MemoryHelper.WriteUnaligned<Int3>(ptr, in expected);

            Int3 ret = MemoryHelper.ReadUnaligned<Int3>(ptr);
            Int3 ret2;
            MemoryHelper.ReadUnaligned<Int3>(ptr, out ret2);

            Int3.AssertEquals(expected, ret);
            Int3.AssertEquals(expected, ret2);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void ReadWriteUnalignedTest3()
        {
            //Test writing/reading from an unaligned ptr using the normal methods

            //Array for 4 structs, with some padding
            uint numBytes = (uint)(MemoryHelper.SizeOf<Int3>() * 4) + 1;
            void* stackPtr = stackalloc byte[(int)numBytes];

            IntPtr ptr = new IntPtr(stackPtr);
            ptr += 1; //Increment by amount of padding
            int sizeInBytes = MemoryHelper.SizeOf<Int3>();

            MemoryHelper.ClearMemory(ptr, 0, numBytes);

            ptr += (sizeInBytes * 2); //Go to "index 2"

            Int3 expected = new Int3(2005, 35, 1015);

            MemoryHelper.Write<Int3>(ptr, in expected);

            Int3 ret = MemoryHelper.Read<Int3>(ptr);
            Int3 ret2;
            MemoryHelper.Read<Int3>(ptr, out ret2);

            Int3.AssertEquals(expected, ret);
            Int3.AssertEquals(expected, ret2);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void ReadWriteTest()
        {
            Int3* stackptr = stackalloc Int3[4];

            IntPtr ptr = new IntPtr(stackptr);
            int sizeInBytes = MemoryHelper.SizeOf<Int3>();

            MemoryHelper.ClearMemory(ptr, 0, (uint) sizeInBytes * 4);

            ptr += (sizeInBytes * 2);

            Int3 expected = new Int3(2005, 35, 1015);

            MemoryHelper.Write<Int3>(ptr, in expected);

            Int3 ret = MemoryHelper.Read<Int3>(ptr);
            Int3 ret2;
            MemoryHelper.Read<Int3>(ptr, out ret2);

            Int3.AssertEquals(expected, ret);
            Int3.AssertEquals(expected, ret2);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void MemCopyTest()
        {
            Int3* stackptr = stackalloc Int3[4];

            Int3[] array = new Int3[4];
            array[0] = new Int3(252, 2, 662);
            array[1] = new Int3(2699, 120, 25626);
            array[2] = new Int3(1928, 35, 120);
            array[3] = new Int3(15001, 3, 15200);

            GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);

            try
            {
                MemoryHelper.MemoryCopy(new IntPtr(stackptr), handle.AddrOfPinnedObject(), (uint)(MemoryHelper.SizeOf<Int3>() * array.Length));

                for(int i = 0; i < array.Length; i++)
                    Int3.AssertEquals(array[i], stackptr[i]);
            }
            finally
            {
                handle.Free();
            }
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void MemCopyUnalignedTest()
        {
            //Add a padding byte at end
            uint numBytes = (uint)(MemoryHelper.SizeOf<Int3>() * 4) + 1;
            void* stackptr = stackalloc byte[(int)numBytes];

            //Increment start by the padding amount
            Int3* ptr = (Int3*)(new IntPtr(stackptr) + 1).ToPointer();

            Int3[] array = new Int3[4];
            array[0] = new Int3(252, 2, 662);
            array[1] = new Int3(2699, 120, 25626);
            array[2] = new Int3(1928, 35, 120);
            array[3] = new Int3(15001, 3, 15200);

            GCHandle handle = GCHandle.Alloc(array, GCHandleType.Pinned);

            try
            {
                MemoryHelper.MemoryCopyUnaligned(new IntPtr(ptr), handle.AddrOfPinnedObject(), (uint)(MemoryHelper.SizeOf<Int3>() * array.Length));

                for (int i = 0; i < array.Length; i++)
                    Int3.AssertEquals(array[i], ptr[i]);
            }
            finally
            {
                handle.Free();
            }
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void ReadWriteArrayTest()
        {
            Int3* stackptr = stackalloc Int3[4];

            Int3[] array = new Int3[4];
            array[0] = new Int3(252, 2, 662);
            array[1] = new Int3(2699, 120, 25626);
            array[2] = new Int3(1928, 35, 120);
            array[3] = new Int3(15001, 3, 15200);

            //Write contents to stack ptr
            MemoryHelper.Write<Int3>(new IntPtr(stackptr), array, 0, array.Length);

            //Check stack ptr
            for (int i = 0; i < array.Length; i++)
                Int3.AssertEquals(array[i], stackptr[i]);

            //Round trip into another array
            Int3[] otherArray = new Int3[4];

            MemoryHelper.Read<Int3>(new IntPtr(stackptr), otherArray, 0, array.Length);

            //Check against original array
            for (int i = 0; i < array.Length; i++)
                Int3.AssertEquals(array[i], otherArray[i]);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void ReadWriteArrayUnalignedTest()
        {
            //Add a padding byte at end
            uint numBytes = (uint)(MemoryHelper.SizeOf<Int3>() * 4) + 1;
            void* stackptr = stackalloc byte[(int)numBytes];

            //Increment start by the padding amount
            Int3* ptr = (Int3*)(new IntPtr(stackptr) + 1).ToPointer();

            Int3[] array = new Int3[4];
            array[0] = new Int3(252, 2, 662);
            array[1] = new Int3(2699, 120, 25626);
            array[2] = new Int3(1928, 35, 120);
            array[3] = new Int3(15001, 3, 15200);

            //Write contents to stack ptr
            MemoryHelper.WriteUnaligned<Int3>(new IntPtr(ptr), array, 0, array.Length);

            //Check stack ptr
            for (int i = 0; i < array.Length; i++)
                Int3.AssertEquals(array[i], ptr[i]);

            //Round trip into another array
            Int3[] otherArray = new Int3[4];

            MemoryHelper.ReadUnaligned<Int3>(new IntPtr(ptr), otherArray, 0, array.Length);

            //Check against original array
            for (int i = 0; i < array.Length; i++)
                Int3.AssertEquals(array[i], otherArray[i]);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void PerformanceTestSingleReads()
        {
            PerformanceTestSingleReadsInternal(false);
        }

        [Fact]
        [Trait("Category", "NET Framework")]
        public unsafe void PerformanceTestSingleReadsCorrectness()
        {
            PerformanceTestSingleReadsInternal(true);
        }

        private unsafe void PerformanceTestSingleReadsInternal(bool testCorrectness)
        {
            int numIterations = 500;
            int numElems = 1000;
            int sizeOf = MemoryHelper.SizeOf<Int3>();
            IntPtr ptr = Marshal.AllocHGlobal(sizeOf * numElems);

            try
            {

                Int3[] expected = new Int3[numElems];
                Random rand = new Random();

                for (int i = 0; i < numElems; i++)
                    expected[i] = new Int3(rand.Next(-1000, 1000), (byte)rand.Next(0, 100), rand.Next(2500, 5500));

                MemoryHelper.Write<Int3>(ptr, expected, 0, numElems);

                Stopwatch watch = new Stopwatch();
                watch.Start();

                if (testCorrectness)
                {
                    for (int itr = 0; itr < numIterations; itr++)
                    {
                        for (int i = 0; i < numElems; i++)
                        {
                            Int3 v = MemoryInterop.TestCommon.MemoryInterop.ReadInline<Int3>((void*)(ptr + (i * sizeOf)));

                            Int3.AssertEquals(expected[i], v);
                        }
                    }
                }
                else
                {
                    for (int itr = 0; itr < numIterations; itr++)
                    {
                        for (int i = 0; i < numElems; i++)
                        {
                            Int3 v = MemoryInterop.TestCommon.MemoryInterop.ReadInline<Int3>((void*)(ptr + (i * sizeOf)));
                        }
                    }
                }

                watch.Stop();

                m_output.WriteLine("MemoryInterop SingleRead x {0} elements x {1} iterations: {2} ms.", numElems, numIterations, watch.ElapsedMilliseconds);

                watch.Restart();


                if (testCorrectness)
                {
                    for (int itr = 0; itr < numIterations; itr++)
                    {
                        for (int i = 0; i < numElems; i++)
                        {
                            Int3 v = Marshal.PtrToStructure<Int3>(ptr + (i * sizeOf)); //New in .net 4.5.1, the one that returns an object did not seem to perform much slower. But lots of boxing!

                            Int3.AssertEquals(expected[i], v);
                        }
                    }
                }
                else
                {
                    for (int itr = 0; itr < numIterations; itr++)
                    {
                        for (int i = 0; i < numElems; i++)
                        {
                            Int3 v = Marshal.PtrToStructure<Int3>(ptr + (i * sizeOf)); //New in .net 4.5.1, the one that returns an object did not seem to perform much slower. But lots of boxing!
                        }
                    }
                }

                watch.Stop();

                m_output.WriteLine("Marshal SingleRead x {0} elements x {1} iterations: {2} ms", numElems, numIterations, watch.ElapsedMilliseconds);
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
        }
    }
}
