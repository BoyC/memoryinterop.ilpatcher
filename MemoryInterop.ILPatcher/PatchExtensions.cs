﻿/*
* Copyright (c) 2017-2020 MemoryInterop.ILPatcher - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Mono.Cecil.Cil;
using System.Collections.Generic;
using System.Runtime.Versioning;

namespace MemoryInterop.ILPatcher
{
    /// <summary>
    /// Extension helper methods.
    /// </summary>
    public static class PatchExtensions
    {
        /// <summary>
        /// Replaces a single instruction with multiple new instructions.
        /// </summary>
        /// <param name="ilGen">ILGenerator</param>
        /// <param name="instructionsToAdd">Instructions to add.</param>
        /// <param name="instructionToReplace">Instruction contained by the ILGenerator at which to replace.</param>
        public static void Inject(this ILProcessor ilGen, IReadOnlyList<Instruction> instructionsToAdd, Instruction instructionToReplace)
        {
            Instruction prevInstruction = instructionToReplace;

            for (int i = 0; i < instructionsToAdd.Count; i++)
            {
                Instruction currInstruction = instructionsToAdd[i];
                ilGen.InsertAfter(prevInstruction, currInstruction);
                prevInstruction = currInstruction;
            }

            ilGen.Remove(instructionToReplace);
        }

        /// <summary>
        /// Resets max stack size based on # of variables defined in the method body.
        /// </summary>
        /// <param name="body">Method body.</param>
        public static void ResetMaxStackSize(this MethodBody body)
        {
            if (body.Variables.Count <= 1)
                body.MaxStackSize = 1;
        }

        /// <summary>
        /// Determine if the framework targets the NET Core.
        /// </summary>
        /// <param name="framework">Framework name.</param>
        /// <returns>True if the target is NETCore, false if otherwise.</returns>
        public static bool IsNetCore(this FrameworkName framework)
        {
            return framework.Identifier.Equals(".NETCoreApp");
        }

        /// <summary>
        /// Determine if the framework targets the NET Framework.
        /// </summary>
        /// <param name="framework">Framework name.</param>
        /// <returns>True if the target is NET Framework, false if otherwise.</returns>
        public static bool IsNetFramework(this FrameworkName framework)
        {
            return framework.Identifier.Equals(".NETFramework");
        }

        /// <summary>
        /// Determine if the framework targets the NET Standard
        /// </summary>
        /// <param name="framework">Framework name.</param>
        /// <returns>True if the target is NET Standardk, false if otherwise.</returns>
        public static bool IsNetStandard(this FrameworkName framework)
        {
            return framework.Identifier.Equals(".NETStandard");
        }
    }
}
