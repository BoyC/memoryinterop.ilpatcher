﻿/*
* Copyright (c) 2017-2020 MemoryInterop.ILPatcher - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;

namespace MemoryInterop.ILPatcher
{
    /// <summary>
    /// Processors for the "MemoryInterop" stub type which define a number of read/write/cast methods for working with pointer types and generics.
    /// </summary>
    public static class MemoryInteropProcessors
    {
        /// <summary>
        /// Stub type name.
        /// </summary>
        public static readonly String TypeName = "MemoryInterop";

        //
        // Method replacement processors. These replace the entire method body.
        //

        /// <summary>
        /// As cast method.
        /// </summary>
        public static readonly String AsName = "As";

        /// <summary>
        /// AsReadonly cast method.
        /// </summary>
        public static readonly String AsReadonlyName = "AsReadonly";

        /// <summary>
        /// AsRef cast method.
        /// </summary>
        public static readonly String AsRefName = "AsRef";

        /// <summary>
        /// Write to (managed) array method.
        /// </summary>
        public static readonly String WriteArrayName = "WriteArray";

        /// <summary>
        /// Write to (managed) array method. Copy from pointer is unaligned.
        /// </summary>
        public static readonly String WriteArrayUnalignedName = "WriteArrayUnaligned";

        /// <summary>
        /// Read from (managed) array method.
        /// </summary>
        public static readonly String ReadArrayName = "ReadArray";

        /// <summary>
        /// Read from (managed) array method. Copy to pointer is unaligned.
        /// </summary>
        public static readonly String ReadArrayUnalignedName = "ReadArrayUnaligned";

        //
        // Inline replacement processors. These replace just the call with a few extra lines of IL (generally).
        //

        /// <summary>
        /// SizeOf inlined method.
        /// </summary>
        public static readonly String SizeOfInlineName = "SizeOfInline";

        /// <summary>
        /// AsPointer cast inlined method.
        /// </summary>
        public static readonly String AsPointerInlineName = "AsPointerInline";

        /// <summary>
        /// AsPointerReadonly cast inlined method.
        /// </summary>
        public static readonly String AsPointerReadonlyInlineName = "AsPointerReadonlyInline";

        /// <summary>
        /// Write inlined method.
        /// </summary>
        public static readonly String WriteInlineName = "WriteInline";

        /// <summary>
        /// Write (unaligned) inlined method.
        /// </summary>
        public static readonly String WriteUnalignedInlineName = "WriteUnalignedInline";

        /// <summary>
        /// Read inlined method.
        /// </summary>
        public static readonly String ReadInlineName = "ReadInline";

        /// <summary>
        /// Read (unaligned) inlined method.
        /// </summary>
        public static readonly String ReadUnalignedInlineName = "ReadUnalignedInline";

        /// <summary>
        /// Memcopy (cpblk) inlined method.
        /// </summary>
        public static readonly String MemCopyInlineName = "MemCopyInline";

        /// <summary>
        /// Memcopy (unaligned cpblk) inlined method.
        /// </summary>
        public static readonly String MemCopyUnalignedInlineName = "MemCopyUnalignedInline";

        /// <summary>
        /// Memset (initblk) inlined method.
        /// </summary>
        public static readonly String MemSetInlineName = "MemSetInline";

        /// <summary>
        /// Memset (unaligned initblk) inlined method.
        /// </summary>
        public static readonly String MemSetUnalignedInlineName = "MemSetUnalignedInline";

        /// <summary>
        /// Adds a list of processors to the specified dictionary, where each list of processors correspond to a single stub type.
        /// </summary>
        /// <param name="processors">Set of processors to add to.</param>
        public static void CreateProcessors(Dictionary<String, IReadOnlyList<PatchMethodProcessor>> processors)
        {
            List<PatchMethodProcessor> memInteropPatcherList = new List<PatchMethodProcessor>();
            processors.Add(TypeName, memInteropPatcherList);

            memInteropPatcherList.Add(CreateAs(false));
            memInteropPatcherList.Add(CreateAs(true));
            memInteropPatcherList.Add(CreateAsRef());
            memInteropPatcherList.Add(CreateWriteArray());
            memInteropPatcherList.Add(CreateReadArray());

            memInteropPatcherList.Add(CreateSizeOfInline());
            memInteropPatcherList.Add(CreateAsPointerInline(false));
            memInteropPatcherList.Add(CreateAsPointerInline(true));
            memInteropPatcherList.Add(CreateWriteInline());
            memInteropPatcherList.Add(CreateReadInline());
            memInteropPatcherList.Add(CreateMemCopyInline());
            memInteropPatcherList.Add(CreateMemSetInline());
        }

        private static PatchMethodProcessor CreateAs(bool isReadOnly)
        {
            return new NamedPatchMethodProcessor(TypeName, (isReadOnly) ? AsReadonlyName : AsName, false, 
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        methodDef.Body.Instructions.Clear();
                        methodDef.Body.Variables.Clear();
                        methodDef.Body.MaxStackSize = 1;
                        methodDef.Body.InitLocals = false;

                        ILProcessor ilGen = methodDef.Body.GetILProcessor();
                        ilGen.Emit(OpCodes.Ldarg_0);
                        ilGen.Emit(OpCodes.Ret);
                    }
                );
        }

        private static PatchMethodProcessor CreateAsRef()
        {
            return new NamedPatchMethodProcessor(TypeName, AsRefName, false,
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        methodDef.Body.Instructions.Clear();
                        methodDef.Body.Variables.Clear();
                        methodDef.Body.MaxStackSize = 1;
                        methodDef.Body.InitLocals = false;

                        ILProcessor ilGen = methodDef.Body.GetILProcessor();

                        if (context.FrameworkName != null && context.FrameworkName.IsNetCore())
                        {
                            //Netcoreapp doesn't need to roundtrip
                            ilGen.Emit(OpCodes.Ldarg_0);
                        }
                        else
                        {
                            //Need to roundtrip via a local to avoid type mismatch on return (not needed in netcoreapp...see S.R.CS.Unsafe
                            methodDef.Body.Variables.Add(new VariableDefinition(new ByReferenceType(context.ImportReference(context.ModuleDefinition.TypeSystem.Int32.Resolve()))));

                            ilGen.Emit(OpCodes.Ldarg_0);
                            ilGen.Emit(OpCodes.Stloc_0);
                            ilGen.Emit(OpCodes.Ldloc_0);
                        }

                        ilGen.Emit(OpCodes.Ret);
                    }
                );
        }

        private static PatchMethodProcessor CreateWriteArray()
        {
            return new NamedPatchMethodProcessor(TypeName, WriteArrayName, false,
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        bool isUnaligned = (instruToPatch.Operand as MethodReference).Name.Equals(WriteArrayUnalignedName);

                        methodDef.Body.Instructions.Clear();
                        methodDef.Body.Variables.Clear();
                        methodDef.Body.MaxStackSize = 1;
                        methodDef.Body.InitLocals = true;

                        ILProcessor ilGen = methodDef.Body.GetILProcessor();
                        TypeReference paramT = methodDef.GenericParameters[0];

                        //local(0) Pinned T
                        methodDef.Body.Variables.Add(new VariableDefinition(new PinnedType(new ByReferenceType(paramT))));

                        //Push(0) pDest
                        ilGen.Emit(OpCodes.Ldarg_0);

                        //fixed(void* pinnedData = &data[startIndexInArray]
                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Ldarg_2);
                        ilGen.Emit(OpCodes.Ldelema, paramT);
                        ilGen.Emit(OpCodes.Stloc_0);

                        //Push (0) pinnedData for memcpy
                        ilGen.Emit(OpCodes.Ldloc_0);

                        //totalSize = sizeof(T) * count
                        ilGen.Emit(OpCodes.Sizeof, paramT);
                        ilGen.Emit(OpCodes.Conv_I4);
                        ilGen.Emit(OpCodes.Ldarg_3);
                        ilGen.Emit(OpCodes.Mul);

                        //Memcpy
                        if(isUnaligned)
                            ilGen.Emit(OpCodes.Unaligned, (byte)1);

                        ilGen.Emit(OpCodes.Cpblk);

                        //Return
                        ilGen.Emit(OpCodes.Ret);
                    },
                (MethodReference methodBeingCalled) =>
                    {
                        return methodBeingCalled.Name.Equals(WriteArrayName) || methodBeingCalled.Name.Equals(WriteArrayUnalignedName);
                    }
                );
        }

        private static PatchMethodProcessor CreateReadArray()
        {
            return new NamedPatchMethodProcessor(TypeName, ReadArrayName, false,
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        bool isUnaligned = (instruToPatch.Operand as MethodReference).Name.Equals(ReadArrayUnalignedName);

                        methodDef.Body.Instructions.Clear();
                        methodDef.Body.Variables.Clear();
                        methodDef.Body.MaxStackSize = 1;
                        methodDef.Body.InitLocals = true;

                        ILProcessor ilGen = methodDef.Body.GetILProcessor();
                        TypeReference paramT = methodDef.GenericParameters[0];

                        //local(0) Pinned T
                        methodDef.Body.Variables.Add(new VariableDefinition(new PinnedType(new ByReferenceType(paramT))));

                        //fixed(void* pinnedData = &data[startIndexInArray])
                        ilGen.Emit(OpCodes.Ldarg_1);
                        ilGen.Emit(OpCodes.Ldarg_2);
                        ilGen.Emit(OpCodes.Ldelema, paramT);
                        ilGen.Emit(OpCodes.Stloc_0);

                        //Push (0) pinnedData for memcpy
                        ilGen.Emit(OpCodes.Ldloc_0);

                        //Push (1) pSrc
                        ilGen.Emit(OpCodes.Ldarg_0);

                        //totalSize = sizeof(T) * count
                        ilGen.Emit(OpCodes.Sizeof, paramT);
                        ilGen.Emit(OpCodes.Conv_I4);
                        ilGen.Emit(OpCodes.Ldarg_3);
                        ilGen.Emit(OpCodes.Mul);

                        //Memcpy
                        if(isUnaligned)
                            ilGen.Emit(OpCodes.Unaligned, (byte)1);

                        ilGen.Emit(OpCodes.Cpblk);

                        //Return
                        ilGen.Emit(OpCodes.Ret);
                    },
                (MethodReference methodBeingCalled) =>
                    {
                        return methodBeingCalled.Name.Equals(ReadArrayName) || methodBeingCalled.Name.Equals(ReadArrayUnalignedName);
                    }
            );
        }

        private static PatchMethodProcessor CreateSizeOfInline()
        {
            return new NamedPatchMethodProcessor(TypeName, SizeOfInlineName, true,
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        ILProcessor ilGen = methodDef.Body.GetILProcessor();

                        TypeReference paramT = ((GenericInstanceMethod)instruToPatch.Operand).GenericArguments[0];
                        Instruction newInstruction = ilGen.Create(OpCodes.Sizeof, paramT);
                        ilGen.Replace(instruToPatch, newInstruction);

                        ilGen.Body.ResetMaxStackSize();
                    }
                );
        }

        private static PatchMethodProcessor CreateAsPointerInline(bool isReadOnly)
        {
            return new NamedPatchMethodProcessor(TypeName, (isReadOnly) ? AsPointerReadonlyInlineName : AsPointerInlineName, true,
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        ILProcessor ilGen = methodDef.Body.GetILProcessor();

                        Instruction newInstruction = ilGen.Create(OpCodes.Conv_U);
                        ilGen.Replace(instruToPatch, newInstruction);

                        ilGen.Body.ResetMaxStackSize();
                    }
                );
        }

        private static PatchMethodProcessor CreateWriteInline()
        {
            return new NamedPatchMethodProcessor(TypeName, WriteInlineName, true,
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        bool isUnaligned = (instruToPatch.Operand as MethodReference).Name.Equals(WriteUnalignedInlineName);

                        ILProcessor ilGen = methodDef.Body.GetILProcessor();

                        TypeReference paramT = ((GenericInstanceMethod)instruToPatch.Operand).GenericArguments[0];
                        List<Instruction> instructions = new List<Instruction>();

                        instructions.Add(Instruction.Create(OpCodes.Ldobj, paramT));

                        if(isUnaligned)
                            instructions.Add(Instruction.Create(OpCodes.Unaligned, (byte) 1));

                        instructions.Add(Instruction.Create(OpCodes.Stobj, paramT));

                        ilGen.Inject(instructions, instruToPatch);
                        ilGen.Body.ResetMaxStackSize();
                    }, 
                (MethodReference methodBeingCalled) =>
                    {
                        return methodBeingCalled.Name.Equals(WriteInlineName) || methodBeingCalled.Name.Equals(WriteUnalignedInlineName);
                    }
                );
        }

        private static PatchMethodProcessor CreateReadInline()
        {
            return new NamedPatchMethodProcessor(TypeName, ReadInlineName, true,
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        bool isUnaligned = (instruToPatch.Operand as MethodReference).Name.Equals(ReadUnalignedInlineName);

                        ILProcessor ilGen = methodDef.Body.GetILProcessor();

                        //Do a little analysis. Some cases the injection replaces the method body (returns a copy of T), but other times it assigns it to a local
                        //variable or a by-ref value. If the latter, we should have a Stobj instruction immediately after the call, so transform our ldobj into a cpobj.
                        bool isCopy = instruToPatch.Next.OpCode == OpCodes.Stobj;

                        TypeReference paramT = ((GenericInstanceMethod)instruToPatch.Operand).GenericArguments[0];
                        List<Instruction> instructions = new List<Instruction>();

                        if (isCopy)
                        {
                            ilGen.Remove(instruToPatch.Next);
          
                            if (isUnaligned)
                                instructions.Add(Instruction.Create(OpCodes.Unaligned, (byte) 1));

                            instructions.Add(Instruction.Create(OpCodes.Ldobj, paramT));
                            instructions.Add(Instruction.Create(OpCodes.Stobj, paramT));
                        }
                        else
                        {
                            if (isUnaligned)
                                instructions.Add(Instruction.Create(OpCodes.Unaligned, (byte)1));
                           
                            instructions.Add(Instruction.Create(OpCodes.Ldobj, paramT));
                        }

                        ilGen.Inject(instructions, instruToPatch);
                        ilGen.Body.ResetMaxStackSize();
                    },
                (MethodReference methodBeingCalled) =>
                    {
                        return methodBeingCalled.Name.Equals(ReadInlineName) || methodBeingCalled.Name.Equals(ReadUnalignedInlineName);
                    }
                );
        }

        private static PatchMethodProcessor CreateMemCopyInline()
        {
            return new NamedPatchMethodProcessor(TypeName, MemCopyInlineName, true,
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        bool isUnaligned = (instruToPatch.Operand as MethodReference).Name.Equals(MemCopyUnalignedInlineName);

                        ILProcessor ilGen = methodDef.Body.GetILProcessor();

                        List<Instruction> instructions = new List<Instruction>();

                        if(isUnaligned)
                            instructions.Add(Instruction.Create(OpCodes.Unaligned, (byte)1));

                        instructions.Add(Instruction.Create(OpCodes.Cpblk));
                        ilGen.Inject(instructions, instruToPatch);
                        ilGen.Body.ResetMaxStackSize();
                    },
                (MethodReference methodBeingCalled) =>
                    {
                        return methodBeingCalled.Name.Equals(MemCopyInlineName) || methodBeingCalled.Name.Equals(MemCopyUnalignedInlineName);
                    }
                );
        }

        private static PatchMethodProcessor CreateMemSetInline()
        {
            return new NamedPatchMethodProcessor(TypeName, MemSetInlineName, true,
                (MethodDefinition methodDef, Instruction instruToPatch, PatcherContext context) =>
                    {
                        bool isUnaligned = (instruToPatch.Operand as MethodReference).Name.Equals(MemSetUnalignedInlineName);

                        ILProcessor ilGen = methodDef.Body.GetILProcessor();

                        List<Instruction> instructions = new List<Instruction>();

                        if (isUnaligned)
                            instructions.Add(Instruction.Create(OpCodes.Unaligned, (byte)1));

                        instructions.Add(Instruction.Create(OpCodes.Initblk));
                        ilGen.Inject(instructions, instruToPatch);
                        ilGen.Body.ResetMaxStackSize();
                    },
                (MethodReference methodBeingCalled) =>
                    {
                        return methodBeingCalled.Name.Equals(MemSetInlineName) || methodBeingCalled.Name.Equals(MemSetUnalignedInlineName);
                    }
                );
        }
    }
}
