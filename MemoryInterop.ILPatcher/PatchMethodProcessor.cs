﻿/*
* Copyright (c) 2017-2020 MemoryInterop.ILPatcher - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;

namespace MemoryInterop.ILPatcher
{
    /// <summary>
    /// Base class for method patching. Each processor is associated with a certain (stub) type it looks for. The IL stream of methods will be analyzed, where we search for
    /// calls to stub methods defined in the stub type. Each processor can choose to either intercept the call (e.g. replace the call with an inlined method, or overwrite the entire function)
    /// or not.
    /// </summary>
    public abstract class PatchMethodProcessor
    {
        private String m_declaringTypeName;
        private bool m_isInlined;

        /// <summary>
        /// Gets the stub type to look for.
        /// </summary>
        public String DeclaringTypeName
        {
            get
            {
                return m_declaringTypeName;
            }
        }

        /// <summary>
        /// Gets if the processor weaves IL code inlined or replaces the entire method body.
        /// </summary>
        public bool IsInlined
        {
            get
            {
                return m_isInlined;
            }
        }

        /// <summary>
        /// Constructs a new <see cref="PatchMethodProcessor"/>.
        /// </summary>
        /// <param name="declaringTypeName">Stub type to process.</param>
        /// <param name="isInlined">Declares if the processor weaves IL code inlined.</param>
        public PatchMethodProcessor(String declaringTypeName, bool isInlined)
        {
            m_declaringTypeName = declaringTypeName;
            m_isInlined = isInlined;
        }

        /// <summary>
        /// Gets whether the specific method call should be intercepted by this processor.
        /// </summary>
        /// <param name="methodBeingCalled">The operand of the call instruction currently inspected.</param>
        /// <returns>True if this processor needs to do work, false otherwise.</returns>
        public abstract bool ShouldInterceptCall(MethodReference methodBeingCalled);

        /// <summary>
        /// Called when the processor determines a method call needs to be intercepted. It is given the instruction (the call) that we will overwrite with new IL.
        /// </summary>
        /// <param name="method">The method that we currently are examining the body of.</param>
        /// <param name="instructionToPatchAt">Instruction inside the method body that we need to overwrite.</param>
        /// <param name="context">Context for logging, importing references, etc.</param>
        public abstract void PatchMethod(MethodDefinition method, Instruction instructionToPatchAt, PatcherContext context);

        /// <summary>
        /// Given a method and a set of processors, this will examine each instruction of the method body, looking for calls to stub methods that need to be intercepted.
        /// </summary>
        /// <param name="method">The method body that we will examine.</param>
        /// <param name="processors">Set of processors to do IL injection.</param>
        /// <param name="context">Context for logging, importing references, etc.</param>
        /// <returns>True if the method body has been modified in anyway, or false if not.</returns>
        public static bool ProcessMethod(MethodDefinition method, IReadOnlyDictionary<String, IReadOnlyList<PatchMethodProcessor>> processors, PatcherContext context)
        {
            if (method == null || processors == null)
                return false;

            if (!method.HasBody)
                return false;

            Mono.Collections.Generic.Collection<Instruction> instructions = method.Body.Instructions;
            bool anyModifications = false;

            for(int i = 0; i < instructions.Count; i++)
            {
                Instruction currInstruction = instructions[i];

                //If a call to a method...look at it.
                if (currInstruction.OpCode != OpCodes.Call || !(currInstruction.Operand is MethodReference))
                    continue;

                MethodReference methodBeingCalled = (MethodReference) currInstruction.Operand;

                //Get any processors that are registered to the type name, which is the stub class that we will be replacing.
                IReadOnlyList<PatchMethodProcessor> processorsForThisType;
                if (!processors.TryGetValue(methodBeingCalled.DeclaringType.Name, out processorsForThisType))
                    continue;

                for(int j = 0; j < processorsForThisType.Count; j++)
                {
                    //If processor can patch, let it do so
                    PatchMethodProcessor processor = processorsForThisType[j];
                    if (processor == null || !processor.ShouldInterceptCall(methodBeingCalled))
                        continue;

                    processor.PatchMethod(method, currInstruction, context);
                    anyModifications = true;

                    //If the method is inlined, keep going. If it replaced the whole method body, doesn't make sense to keep running so early out.
                    if (!processor.IsInlined)
                        return true;
                }
            }

            return anyModifications;
        }
    }

    /// <summary>
    /// Generic implementation of a patch processor. It requires the stub type and a specific method to look for, and delegates to patch the method.
    /// </summary>
    public class NamedPatchMethodProcessor : PatchMethodProcessor
    {
        private String m_methodNameToIntercept;
        private Func<MethodReference, bool> m_shouldInterceptOverride;
        private Action<MethodDefinition, Instruction, PatcherContext> m_patchMethod;

        /// <summary>
        /// Gets the name of the stub method to replace.
        /// </summary>
        public String MethodNameToIntercept
        {
            get
            {
                return m_methodNameToIntercept;
            }
        }

        /// <summary>
        /// Constructs a new <see cref="NamedPatchMethodProcessor"/>.
        /// </summary>
        /// <param name="typeName">Name of the stub type to look for.</param>
        /// <param name="methodNameToIntercept">Name of the stub method (declared in the type) to look for.</param>
        /// <param name="isInlined">Declares if the processor weaves IL code inlined.</param>
        /// <param name="patchMethod">Delegate that contains the patching logic.</param>
        /// <param name="shouldInterceptOverride">Optional delegate to override logic of how it is determined if a method call should be intercepted.</param>
        public NamedPatchMethodProcessor(String typeName, String methodNameToIntercept, bool isInlined, Action<MethodDefinition, Instruction, PatcherContext> patchMethod, Func<MethodReference, bool> shouldInterceptOverride = null)
            : base(typeName, isInlined)
        {
            m_methodNameToIntercept = methodNameToIntercept;
            m_shouldInterceptOverride = shouldInterceptOverride;
            m_patchMethod = patchMethod;
        }

        /// <summary>
        /// Gets whether the specific method call should be intercepted by this processor.
        /// </summary>
        /// <param name="methodBeingCalled">The operand of the call instruction currently inspected.</param>
        /// <returns>True if this processor needs to do work, false otherwise.</returns>
        public override bool ShouldInterceptCall(MethodReference methodBeingCalled)
        {
            if (m_shouldInterceptOverride != null)
                return m_shouldInterceptOverride(methodBeingCalled);

            return methodBeingCalled.Name.Equals(m_methodNameToIntercept);
        }

        /// <summary>
        /// Called when the processor determines a method call needs to be intercepted. It is given the instruction (the call) that we will overwrite with new IL.
        /// </summary>
        /// <param name="method">The method that we currently are examining the body of.</param>
        /// <param name="instructionToPatchAt">Instruction inside the method body that we need to overwrite.</param>
        /// <param name="context">Context for logging, importing references, etc.</param>
        public override void PatchMethod(MethodDefinition method, Instruction instructionToPatchAt, PatcherContext context)
        {
            m_patchMethod(method, instructionToPatchAt, context);
        }
    }
}
