﻿/*
* Copyright (c) 2017-2020 MemoryInterop.ILPatcher - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Runtime.Versioning;

namespace MemoryInterop.ILPatcher
{
    /// <summary>
    /// Unified context for patch processors.
    /// </summary>
    public class PatcherContext
    {
        private ModuleDefinition m_module;
        private IPatcherLogger m_logger;
        private Dictionary<String, TypeReference> m_importedTypes;
        private Dictionary<String, MethodReference> m_importedMethods;
        private Dictionary<String, FieldReference> m_importedFields;
        private FrameworkName m_frameworkName;

        /// <summary>
        /// Get the current module that is being processed.
        /// </summary>
        public ModuleDefinition ModuleDefinition
        {
            get
            {
                return m_module;
            }
        }

        /// <summary>
        /// Gets the target framework of the current module.
        /// </summary>
        public FrameworkName FrameworkName
        {
            get
            {
                return m_frameworkName;
            }
        }

        /// <summary>
        /// Gets the logger to log messages.
        /// </summary>
        public IPatcherLogger Logger
        {
            get
            {
                return m_logger;
            }
        }

        /// <summary>
        /// Constructs a new <see cref="PatcherContext"/>.
        /// </summary>
        /// <param name="module">Module that will be processed.</param>
        /// <param name="logger">Logger for messages.</param>
        public PatcherContext(ModuleDefinition module, IPatcherLogger logger)
        {
            m_module = module;
            m_logger = logger;
            m_importedTypes = new Dictionary<String, TypeReference>();
            m_importedMethods = new Dictionary<String, MethodReference>();
            m_importedFields = new Dictionary<String, FieldReference>();
            m_frameworkName = GetTargetFrameworkName(module.Assembly);
        }

        /// <summary>
        /// Imports a reference to the specified type.
        /// </summary>
        /// <param name="typeDef">Type</param>
        /// <returns>Imported reference</returns>
        public TypeReference ImportReference(TypeDefinition typeDef)
        {
            if (typeDef == null)
                return null;

            TypeReference typeRef;
            if (m_importedTypes.TryGetValue(typeDef.FullName, out typeRef))
                return typeRef;

            typeRef = m_module.ImportReference(typeDef);
            m_importedTypes.Add(typeDef.FullName, typeRef);

            return typeRef;
        }

        /// <summary>
        /// Imports a reference to the specified method.
        /// </summary>
        /// <param name="methodDef">Method</param>
        /// <returns>Imported reference</returns>
        public MethodReference ImportReference(MethodDefinition methodDef)
        {
            if (methodDef == null)
                return null;

            MethodReference methodRef;
            if (m_importedMethods.TryGetValue(methodDef.FullName, out methodRef))
                return methodRef;

            methodRef = m_module.ImportReference(methodDef);

            return methodDef;
        }

        /// <summary>
        /// Imports a reference to the specified field.
        /// </summary>
        /// <param name="fieldDef">Field</param>
        /// <returns>Imported reference</returns>
        public FieldReference ImportReference(FieldDefinition fieldDef)
        {
            if (fieldDef == null)
                return null;

            FieldReference fieldRef;
            if (m_importedFields.TryGetValue(fieldDef.FullName, out fieldRef))
                return fieldRef;

            fieldRef = m_module.ImportReference(fieldDef);

            return fieldRef;
        }

        private FrameworkName GetTargetFrameworkName(AssemblyDefinition assemblyDef)
        {
            Mono.Collections.Generic.Collection<CustomAttribute> attributes = assemblyDef.CustomAttributes;
            foreach (CustomAttribute attr in attributes)
            {
                if (attr.Constructor.FullName.Contains("System.Runtime.Versioning.TargetFrameworkAttribute"))
                    return new FrameworkName(attr.ConstructorArguments[0].Value.ToString());
            }

            //Fallback to checking the main module's runtime version
            switch (assemblyDef.MainModule.Runtime)
            {
                case TargetRuntime.Net_1_0:
                    return new FrameworkName(".NETFramework", new Version(1, 0));
                case TargetRuntime.Net_1_1:
                    return new FrameworkName(".NETFramework", new Version(1, 1));
                case TargetRuntime.Net_2_0:
                    return new FrameworkName(".NETFramework", new Version(2, 0));
                case TargetRuntime.Net_4_0:
                    return new FrameworkName(".NETFramework", new Version(4, 0));
            }

            return null;
        }
    }
}
